package org.bitbucket;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The main class of WoolMark
 * 
 * @author runne
 */
public class WoolMark extends Frame {
    private static final long serialVersionUID = -1613358520917634660L;
    public static final int CANVAS_WIDTH = 120;
    public static final int CANVAS_HEIGHT = 120;

    public WoolMark() {
        super("WoolMark");
        WoolMarkCanvas canvas = new WoolMarkCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);

        add(canvas);
        pack();
        setResizable(false);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    public static void main(String args[]) {
        new WoolMark().setVisible(true);
    }
}
