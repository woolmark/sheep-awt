package org.bitbucket;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.imageio.ImageIO;

/**
 * WoolMark Canvas.
 * 
 * @author runne
 */
class WoolMarkCanvas extends Canvas implements Runnable, MouseListener {
    private static final long serialVersionUID = 6736777218751550470L;
    private static final Color SKY_COLOR = new Color(150, 150, 255);
    private static final Color GROUND_COLOR = new Color(100, 255, 100);
    private static final Color MESSAGE_COLOR = Color.BLACK;
    private static final String MESSAGE_FORMAT = "羊が%d匹";
    private static final String FENCE_IMAGE = "/org/bitbucket/fence.png";
    private static final int MESSAGE_X = 5;
    private static final int MESSAGE_Y = 15;
    private Image bufferImage;
    private int width;
    private int height;
    private int fence_x;
    private int fence_y;

    public static final int INITIAL_SHEEP_NUM = 1;
    public static final int SKY_HEIGHT = 50;
    public static final int SPEED = 60;

    /*
     * the frame image.
     */
    private static Image fence;

    /**
     * key flag.
     */
    private boolean isMousePressed;

    private Shepherd shepherd;

    static {
        try {
            fence = ImageIO.read(WoolMarkCanvas.class.getResourceAsStream(FENCE_IMAGE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public WoolMarkCanvas(int width, int height) {
        this.width = width;
        this.height = height;

        fence_x = (this.width - fence.getWidth(null)) / 2;
        fence_y = this.height - fence.getHeight(null);

        setSize(width, height);
        shepherd = new Shepherd(this.width, this.height);
        for (int i = 0; i < INITIAL_SHEEP_NUM; i++) {
            shepherd.addSheep();
        }

        Runnable[] runnables = {
                this, shepherd
        };
        ScheduledExecutorService e = Executors.newScheduledThreadPool(runnables.length);
        for (Runnable r : runnables) {
            e.scheduleAtFixedRate(r, 0, SPEED, MILLISECONDS);
        }

        addMouseListener(this);
    }

    /**
     * run method.
     */
    public void run() {
        if (isMousePressed) {
            shepherd.addSheep();
        }
        repaint();
    }

    public void paint(Graphics g) {
        update(g);
    }

    public synchronized void update(Graphics g) {
        if (bufferImage == null) {
            bufferImage = createImage(width, height);
        }

        Graphics bg = bufferImage.getGraphics();

        bg.setColor(GROUND_COLOR);
        bg.fillRect(0, 0, width, height);
        bg.setColor(SKY_COLOR);
        bg.fillRect(0, 0, width, SKY_HEIGHT);

        bg.drawImage(fence, fence_x, fence_y, null);

        for (Sheep s : shepherd.getSheep()) {
            bg.drawImage(s.getImage(), s.getX(), s.getY(), null);
        }

        bg.setColor(MESSAGE_COLOR);
        bg.drawString(String.format(MESSAGE_FORMAT, shepherd.getSheepNumber()), MESSAGE_X, MESSAGE_Y);

        g.drawImage(bufferImage, 0, 0, null);
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        isMousePressed = true;
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        isMousePressed = false;
    }
}
