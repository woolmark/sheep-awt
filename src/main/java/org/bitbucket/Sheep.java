package org.bitbucket;

import static org.bitbucket.Sheep.SheepState.INACTIVE;
import static org.bitbucket.Sheep.SheepState.JUMP_DOWN;
import static org.bitbucket.Sheep.SheepState.JUMP_UP;
import static org.bitbucket.Sheep.SheepState.RUN;
import static org.bitbucket.WoolMarkCanvas.SKY_HEIGHT;

import java.awt.Image;
import java.util.Observable;
import java.util.Random;

import javax.imageio.ImageIO;

public class Sheep extends Observable {
    private static final int SHEEP_IMAGE_HEIGHT;
    private static final int DELTA_Y = 3;
    private static final int FENCE_BOTTOM_X = 70;
    private static final int FENCE_SPACE = 4;
    private static final String SHEEP_IMAGE01 = "/org/bitbucket/sheep00.png";
    private static final String SHEEP_IMAGE02 = "/org/bitbucket/sheep01.png";
    private static final Random r = new Random();
    private static final Image sheepImages[] = new Image[2];
    private int x;
    private int y;
    private int jump_x;
    private int canvasWidth;
    private int canvasHeight;
    private boolean stretch;
    private SheepState state = INACTIVE;
    
    public static final int SHEEP_IMAGE_WIDTH;
    public static final int DELTA_X = 5;

    static {
        try {
            sheepImages[0] = ImageIO.read(Sheep.class.getResourceAsStream(SHEEP_IMAGE01));
            sheepImages[1] = ImageIO.read(Sheep.class.getResourceAsStream(SHEEP_IMAGE02));
        } catch (Exception e) {
            e.printStackTrace();
        }
        SHEEP_IMAGE_WIDTH = sheepImages[0].getWidth(null);
        SHEEP_IMAGE_HEIGHT = sheepImages[0].getHeight(null);
    }

    private int calcJumpX(int initY) {
        initY -= (canvasHeight - SKY_HEIGHT);
        return (FENCE_BOTTOM_X - DELTA_Y * initY / FENCE_SPACE);
    }

    public Sheep(int canvasWidth, int canvasHeight) {
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImage() {
        stretch = !stretch;
        if (state == JUMP_UP || state == JUMP_DOWN || stretch) {
            return sheepImages[0];
        } else {
            return sheepImages[1];
        }
    }

    public void update() {
        SheepState currentState = state;
        x -= DELTA_X;
        if (x > jump_x) {
            state = RUN;
        } else if (x > jump_x - SHEEP_IMAGE_WIDTH) {
            state = JUMP_UP;
            y -= DELTA_Y;
        } else if (x > jump_x - 2 * SHEEP_IMAGE_WIDTH) {
            state = JUMP_DOWN;
            y += DELTA_Y;
        } else if (x > -1 * SHEEP_IMAGE_WIDTH) {
            state = RUN;
        } else {
            state = INACTIVE;
        }
        if (currentState != state) {
            setChanged();
            notifyObservers(state);
        }
    }

    public void resurrection() {
        x = canvasWidth + SHEEP_IMAGE_WIDTH;
        y = SKY_HEIGHT + r.nextInt(canvasHeight - SKY_HEIGHT - SHEEP_IMAGE_HEIGHT);
        jump_x = calcJumpX(y);
    }

    public enum SheepState {
        INACTIVE, RUN, JUMP_UP, JUMP_DOWN
    }
}
