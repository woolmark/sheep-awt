package org.bitbucket;

import static org.bitbucket.Sheep.DELTA_X;
import static org.bitbucket.Sheep.SHEEP_IMAGE_WIDTH;
import static org.bitbucket.WoolMarkCanvas.INITIAL_SHEEP_NUM;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.bitbucket.Sheep.SheepState;

public class Shepherd implements Observer, Runnable {
    private static final Sheep[] EMPTY_SHEEP_ARRAY = new Sheep[0];
    private static final List<Sheep> activeSheep = Collections.synchronizedList(new ArrayList<Sheep>());
    private static final List<Sheep> inactiveSheep = Collections.synchronizedList(new ArrayList<Sheep>());
    private int sheepNumber = 0;

    public Shepherd(int width, int height) {
        int sheepMax = (width + 2 * SHEEP_IMAGE_WIDTH + (DELTA_X - 1)) / DELTA_X + INITIAL_SHEEP_NUM;
        for (int i = 0; i < sheepMax; i++) {
            inactiveSheep.add(new Sheep(width, height));
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        SheepState state = (SheepState) arg;
        Sheep s = (Sheep) o;
        switch (state) {
        case JUMP_DOWN:
            sheepNumber++;
            break;
        case INACTIVE:
            activeSheep.remove(s);
            inactiveSheep.add(s);
            if (activeSheep.size() == 0) {
                addSheep();
            }
            break;
        default:
            break;
        }
    }

    public int getSheepNumber() {
        return sheepNumber;
    }

    public void addSheep() {
        Sheep s = inactiveSheep.remove(0);
        s.resurrection();
        activeSheep.add(s);
        s.addObserver(this);
    }

    public Sheep[] getSheep() {
        return activeSheep.toArray(EMPTY_SHEEP_ARRAY);
    }

    @Override
    public void run() {
        Sheep[] sheep = activeSheep.toArray(EMPTY_SHEEP_ARRAY);
        for (Sheep s : sheep) {
            s.update();
        }
    }
}
