# How to build WoolMarkAwt
1. Resolve dependence 

    $ chmod 755 gradlew
    $ ./gradlew

2. Build 

    $ gradle build

3. Execution

    $ java -classpath build/libs/sheep-awt-1.0.0.jar org.bitbucket.WoolMark

## Appendix

1. How to import eclipse?

    $ ./gradlew eclipse
    .project and .classpath are generated. Please import sheep-awt by eclipse.
